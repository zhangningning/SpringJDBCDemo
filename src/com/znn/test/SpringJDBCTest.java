package com.znn.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.znn.dao.UserDao;
import com.znn.vo.User;

public class SpringJDBCTest {

	@Test
	public void test() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
		UserDao dao = (UserDao) applicationContext.getBean("userDao");
//		List<User> list = dao.query();
//		System.out.println(list);
		User user= new User();
//		user.setId(1);
		user.setName("adminn");
		user.setAge(22);
//		affectNum = dao.save(user);
//		affectNum = dao.update(user);
//		affectNum = dao.delete(1);
//		System.out.println(affectNum);
		
		User u = dao.queryAUser(2);
		System.out.println(u);
	}

}
